/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fernnx2.inizilizerspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author fernando
 */
@Entity
@Table(name="home")
public class Home {
    
    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="saludo")
	private String saludo;

    public Home(int id, String saludo) {
        this.id = id;
        this.saludo = saludo;
    }
    
    public Home(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSaludo() {
        return saludo;
    }

    public void setSaludo(String saludo) {
        this.saludo = saludo;
    }
        
        
        
	@Override
	public String toString() {
		return "home [id=" + id + ", saludo=" + saludo +"]";
	}
}
