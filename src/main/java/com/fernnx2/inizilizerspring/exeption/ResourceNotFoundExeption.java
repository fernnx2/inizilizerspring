/*

 */
package com.fernnx2.inizilizerspring.exeption;

/**
 *
 * @author fernando
 */
public class ResourceNotFoundExeption extends Exception {
    
    private static final long serialVersionUID = 1L;

    public ResourceNotFoundExeption(Object resourId) {
        super(resourId != null ? resourId.toString() : null);
    }
    
}
