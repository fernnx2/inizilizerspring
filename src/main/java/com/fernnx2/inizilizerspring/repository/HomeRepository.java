/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fernnx2.inizilizerspring.repository;

import com.fernnx2.inizilizerspring.model.Home;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fernando
 */
@Repository("userRepository")
public interface HomeRepository extends JpaRepository<Home,Integer> {
    
}
